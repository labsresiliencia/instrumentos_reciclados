# Instrumentos Reciclados

Instrumetnos Reciclados es un dispositivo electrónico
que permite utilizar desechos metálicos para generar
sonidos con Arduino.

Este repositorio contiene el código fuente e 
instrucciones de armado del proyecto denominado 
"Instrumentos Reciclados" desarrollado como parte de los 
Laboratorios de Resiliencia Comunitarios.

Prototipo elaborado por:
* Maria Teresa Melendez
* Pedro Antonio Sánchez
* Meyvelin Sujey Montano

## Lista de Materiales
Para construir este proyecto necesitas los siguientes materiales:

| Nombre                          | Cantidad |
|---------------------------------|----------|
| Cartón de caja reciclada        |        1 |
| Aluminio o piezas de metal      |        7 |
| Pegamento                       |        1 |
| Breadboard                      |        1 |
| Arduino UNO R3                  |        1 |
| Resistencias de 10K             |        7 |
| Bocina                          |        1 |

## Diagrama de Conexión
![](./Docu/Esquematico_bb.png)

## Guía de Conexion en Breadboard
Las conexiónes que comienzan con **BB** se conectan en 
la Breadboard. Las que comienzan con **AR** se conectan 
al Arduino. Las que comienzan con **ME** indican placas
metálicas o cinta conductora.

| Componente                         |        |         |
|------------------------------------|--------|---------|
| Resistencia 1                      | BB-D20 | BB-F20  |
| Resistencia 2                      | BB-D25 | BB-F25  |
| Resistencia 3                      | BB-D30 | BB-F30  |
| Resistencia 4                      | BB-D35 | BB-F35  |
| Resistencia 5                      | BB-D40 | BB-F40  |
| Resistencia 6                      | BB-D45 | BB-F45  |
| Resistencia 7                      | BB-D50 | BB-F50  |
| Jumper 1                           | BB-A20 | BB-V-   |
| Jumper 2                           | BB-A25 | BB-V-   |
| Jumper 3                           | BB-A30 | BB-V-   |
| Jumper 4                           | BB-A35 | BB-V-   |
| Jumper 5                           | BB-A40 | BB-V-   |
| Jumper 6                           | BB-A45 | BB-V-   |
| Jumper 7                           | BB-A50 | BB-V-   |
| Jumper 8                           | BB-G20 | AR-9    |
| Jumper 9                           | BB-G25 | AR-8    |
| Jumper 10                          | BB-G30 | AR-7    |
| Jumper 11                          | BB-G35 | AR-6    |
| Jumper 12                          | BB-G40 | AR-5    |
| Jumper 13                          | BB-G45 | AR-4    |
| Jumper 14                          | BB-G50 | AR-3    |
| Jumper 15                          | BB-V-  | AR-2    |
| Jumper 16                          | AR-5V  | BB-V+   |
| Bocina                             | BB-V+  | AR-13   |
| Jumper 17                          | BB-J20 | ME-1    |
| Jumper 18                          | BB-J25 | ME-2    |
| Jumper 19                          | BB-J30 | ME-3    |
| Jumper 20                          | BB-J35 | ME-4    |
| Jumper 21                          | BB-J40 | ME-5    |
| Jumper 22                          | BB-J45 | ME-6    |
| Jumper 23                          | BB-J50 | ME-7    |

## Información para Colaborar con este Proyecto
Para colaborar con este proyecto solicita acceso al canal
oficial de Slack en [https://labsresiliencia.slack.com](https://labsresiliencia.slack.com) 
y únete al canal ***#propuesta_e3***.
