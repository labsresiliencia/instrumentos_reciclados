// Importar la
#include <CapacitiveSensor.h>
// Pin donde estará conectado la bocina
#define speaker 13

CapacitiveSensor   cs_2_5 = CapacitiveSensor(10, 3);       //declaración de pines de conexión de resistencias
CapacitiveSensor   cs_2_6 = CapacitiveSensor(10, 4);
CapacitiveSensor   cs_2_7 = CapacitiveSensor(10, 6);
CapacitiveSensor   cs_2_8 = CapacitiveSensor(10, 7);
CapacitiveSensor   cs_2_9 = CapacitiveSensor(10, 5);
CapacitiveSensor   cs_2_10 = CapacitiveSensor(10, 8);

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  long total =  cs_2_5.capacitiveSensor(30);
  long total2 =  cs_2_6.capacitiveSensor(30);
  long total3 =  cs_2_7.capacitiveSensor(30);
  long total4 =  cs_2_8.capacitiveSensor(30);
  long total5 =  cs_2_9.capacitiveSensor(30);
  long total6 =  cs_2_10.capacitiveSensor(30);

  Serial.print(total);
  Serial.print("\t");
  Serial.print(total2);
  Serial.print("\t");
  Serial.print(total3);
  Serial.print("\t");
  Serial.print(total4);
  Serial.print("\t");
  Serial.print(total5);
  Serial.print("\t");
  Serial.print(total6);
  Serial.print("\t");
  Serial.print("\n");

  if (total > 10000) tone(speaker, 523.25);
  if (total2 > 10000) tone(speaker, 587.33);
  if (total3 > 1000) tone(speaker, 659.26);
  if (total4 > 1000) tone(speaker, 783.99);
  if (total5 > 1000) tone(speaker, 880);
  if (total6 > 1000) tone(speaker, 987.77);
  if ((total <= 10000) && (total2 <= 10000) && (total3 <= 10000) && (total4 <= 10000) && (total5 <= 10000) && (total6 <= 10000))
    noTone(speaker);
}
